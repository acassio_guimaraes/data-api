import os

from app import app

# Costumers blueprints
#from app.blueprints.receive_file import receive_file
from app.blueprints.search import process_search
from app.blueprints.process import receive_file

app.register_blueprint(receive_file, url_prefix='/v1/data/process')
app.register_blueprint(process_search, url_prefix='/v1/data/search')

#app.register_blueprint(fill_search_fields, url_prefix='v1/data')

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=False)
