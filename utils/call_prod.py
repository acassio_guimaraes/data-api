#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pdfprod import PdfProdParser as prod
import pandas as pd

file_in = r'C:\Users\Acássio\Google Drive\Tamadas\inputs\germinação\Germinação 01 jan 2016-14 mar 2017.pdf'
        

fields_dict =     {"ordem":1, "semente":2, "lote":3,"prod":4,
                   "dt_lote":5,"semana_lote":6,"dt_sem":7,
                   "semana_sem":8,"dt_entrega":9,"semana_entrega":10,
                   "band_semeada":11,"muda_semeada":12,"band_estimada":13,
                   "muda_estimada":14,"perc_estimada":15,"band_real":16,
                   "muda_real":17, "perc_real":18}
                  
obj = prod(file_in, fields_dict)
data_frame = obj.provider('prod')

writer = pd.ExcelWriter(r'C:\Users\Acássio\Desktop\Consultoria\tamadas\testes\germina_teste2.xlsx')
data_frame.to_excel(writer,'Sheet1')
writer.save()

