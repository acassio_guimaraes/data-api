from celery import Celery

app = Celery('celery_example', broker='redis://localhost')

@app.task
def add():
    lista = list()
    for i in range(100000):
        lista.append(i)
    return lista
