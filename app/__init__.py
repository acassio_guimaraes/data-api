# Import flask and dependencies
from flask import Flask

# Define the WSGI application object
app = Flask(__name__)
app.config.from_object('config')
