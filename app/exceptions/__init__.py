class ValidationError(Exception):
    status_code = 422

    def __init__(self, message = None, fields=[]):
        self.fields  = fields
        self.message = message

    def to_dict(self):
        response = { 'fields': self.fields, 'message': self.message }

class AuthenticationError(Exception):
    status_code = 401

    pass

class AuthorizationError(Exception):
    status_code = 403

    pass

class NotFoundError(Exception):
    status_code = 404

    pass
