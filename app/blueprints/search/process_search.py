from app import app
from flask import jsonify, Blueprint, request, abort, g, Response
from json import dumps
from app.els import query_service_level
from app.els import query_customer_revenue
from app.els import return_autofill_list
from app.els import query_customer_payment
from app.els import return_customer_cluster
from app.els import return_top_product
from app.els import query_customer_cancel
process_search = Blueprint('process_search', __name__)


@process_search.route("/<int:company_id>", methods=['GET'])
def search(company_id=None, foreing_vendor_id=None):
    '''
    Process each file depend on the admin
    '''

    list_response = list()

    if company_id == 1:
        data = dict(request.args.items())
        vendor = None
        customer = None
        product = None

        for item in data:
            if item == "vendor":
                vendor = data[item]
            elif item == "customer":
                customer = data[item]
            elif item == "product":
                product = data[item]

        receive_cr = query_customer_revenue(vendor, customer, product)
        list_response.append(receive_cr)
        receive_pd, receive_bl = query_customer_payment(vendor, customer)
        list_response.append(receive_pd)
        list_response.append(receive_bl)
        receive_cancel = query_customer_cancel(vendor,customer,product)
        list_response.append(receive_cancel)
        receive_sl = query_service_level(product)
        list_response.append(receive_sl)
        
    return jsonify(list_response)


@process_search.route("/autofill/<int:company_id>", methods=['GET'])
def auto_fill(company_id=None):
    '''
    Process each file depend on the admin
    '''
    receive = return_autofill_list()

    return jsonify(receive)


@process_search.route("/cluster/<int:company_id>", methods=['GET'])
def cluster(company_id=None):
    '''
    Process each file depend on the admin
    '''
    list_response = list()

    if company_id == 1:
        data = dict(request.args.items())
        vendor = None
        customer = None
        product = None

        for item in data:
            if item == "vendor":
                vendor = data[item]
            elif item == "customer":
                customer = data[item]
            elif item == "product":
                product = data[item]

    customer_cluster = return_customer_cluster(vendor, customer)
    list_response.append(customer_cluster)
    product_list = return_top_product(vendor, customer, product)
    list_response.append(product_list)
    return jsonify(list_response)
