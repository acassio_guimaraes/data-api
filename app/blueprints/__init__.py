from app   import app
from flask import jsonify, request
from app.exceptions import ValidationError, AuthenticationError, NotFoundError

# CORS treatments
@app.after_request
def apply_cors(response):
    if request.method == "OPTIONS":
        response.status = "204 NO CONTENT"

    response.headers["Access-Control-Allow-Origin"]  = "*"
    response.headers["Access-Control-Allow-Headers"] = "Content-Type,X-Api-Key"
    response.headers["Access-Control-Allow-Methods"] = "POST, GET, PUT, DELETE"

    return response

# Handle 404 errors
@app.errorhandler(404)
def not_found(error):
    return jsonify({}), 404

@app.errorhandler(NotFoundError)
def handle_not_found_error(NotFoundError):
    return jsonify({}), 404

# Handle ValidationError
@app.errorhandler(ValidationError)
def handle_validation_error(error):
    fields = list(set(error.fields))

    return jsonify({ 'fields': fields }), error.status_code

# Handle ValidationError
@app.errorhandler(AuthenticationError)
def handle_validation_error(error):
    return jsonify({}), error.status_code
