from json  import dumps
from flask import Response

from datetime import datetime

class Representer:
    res_body  = []
    exposures = []

    def __init__(self, response, force_show=[]):
        self.res_body   = []
        self.force_show = force_show

        if type(response) is list:
            for item in response:
                self.res_body.append(self.adjust(item))
        else:
            self.res_body = self.adjust(response)


    def adjust(self, instance):
        item = {}

        for attr in self.exposures + self.force_show:
            if type(attr) is list:
                item[attr[1]] = attr[0](getattr(instance, attr[1])).res_body
            elif attr in dir(instance):
                item[attr] = getattr(instance, attr)

                if isinstance(item[attr], datetime):
                    item[attr] = item[attr].isoformat()

        return item

    def respond(self):
        return Response(dumps(self.res_body), mimetype='application/json')
