from app.representer.representer import Representer

class FileRepresenter(Representer):
    exposures = [
        'id',
        'name',
        'status',
        'description',
        'filepath',
        's3bucket'
    ]
