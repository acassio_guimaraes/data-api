from elasticsearch_dsl import Index
from elasticsearch_dsl import DocType
from elasticsearch_dsl import Nested
from elasticsearch_dsl import Integer
from elasticsearch_dsl import String
from elasticsearch_dsl import Date
from elasticsearch_dsl import Double
from elasticsearch_dsl import Text
from elasticsearch_dsl import Keyword
from elasticsearch_dsl.connections import connections
from app import app
# Define a default Elasticsearch client


class CustomerRevenue(DocType):
    '''
    Class to map process
    '''
    company_id = Integer()
    foreing_customer_id = Integer()
    customer_name = Text(analyzer='standard')
    foreing_vendor_id = Integer()
    vendor_name = Text(analyzer='standard')
    date = Date()
    value = Double()
    product_name = Text(analyzer='standard')
    product_agreg = String()
    tray = Integer()
    product_detail = String()
    product_keyword = Keyword()

    class Meta:
        '''
        Meta fields: index and doc_type
        '''
        index = 'tamadas_revenue'
        doc_type = 'customer_revenue'

    def save(self, ** kwargs):
        '''
        Commit
        '''
        return super().save(** kwargs)


class CustomerPayment(DocType):
    '''
    Class to map process
    '''
    company_id = Integer()
    foreing_customer_id = Integer()
    customer_name = Text(analyzer='standard')
    foreing_vendor_id = Integer()
    vendor_name = Text(analyzer='standard')
    date = Date()
    due_value = Double()
    paid_value = Double()
    balance = Double()
    balance_past_due = Double()
    balance_target = Double()
    past_due_target = Double()
    em_dia = Double()
    fx_5_a_15 = Double()
    fx_15_a_30 = Double()
    fx_30_a_60 = Double()
    fx_60_a_120 = Double()
    fx_120_a_180 = Double()
    fx_180_a_360 = Double()
    fx_maior_360 = Double()

    class Meta:
        '''
        Meta fields: index and doc_type
        '''
        index = 'tamadas_payment'
        doc_type = 'customer_payment'

    def save(self, ** kwargs):
        '''
        Commit
        '''
        return super().save(** kwargs)


class CustomerCluster(DocType):
    '''
    Class to map process
    '''
    company_id = Integer()
    foreing_customer_id = Integer()
    customer_name = Text(analyzer='standard')
    foreing_vendor_id = Integer()
    vendor_name = Text(analyzer='standard')
    customer_cluster = String()
    customer_factor = Double()

    class Meta:
        '''
        Meta fields: index and doc_type
        '''
        index = 'tamadas_cluster'
        doc_type = 'customer_cluster'

    def save(self, ** kwargs):
        '''
        Commit
        '''
        return super().save(** kwargs)


class ServiceLevel(DocType):
    '''
    Class to map process
    '''
    company_id = Integer()
    date = Date()
    product_name = Text(analyzer='standard')
    product_agreg = String()
    tray = Integer()
    product_detail = String()
    value_seed = Integer()
    value_final = Integer()

    class Meta:
        '''
        Meta fields: index and doc_type
        '''
        index = 'tamadas_service'
        doc_type = 'service_level'

    def save(self, ** kwargs):
        '''
        Commit
        '''
        return super().save(** kwargs)


class CustomerCancel(DocType):
    company_id = Integer()
    foreing_customer_id = Integer()
    customer_name = Text(analyzer='standard')
    foreing_vendor_id = Integer()
    vendor_name = Text(analyzer='standard')
    product_name = Text(analyzer='standard')
    date = Date()
    canceled_qt = Integer()
    plan_qt = Integer()

    class Meta:
        '''
        Meta fields: index and doc_type
        '''
        index = 'tamadas_cancel'
        doc_type = 'customer_cancel'

    def save(self, ** kwargs):
        '''
        Commit
        '''
        return super().save(** kwargs)


class VendorList(DocType):
    '''
    Class to map process
    '''
    company_id = Integer()
    vendor_name = Text(analyzer='standard')

    class Meta:
        '''
        Meta fields: index and doc_type
        '''
        index = 'tamadas_vendor_list'
        doc_type = 'vendor_list'

    def save(self, ** kwargs):
        '''
        Commit
        '''
        return super().save(** kwargs)


class CustomerList(DocType):
    '''
    Class to map process
    '''
    company_id = Integer()
    customer_name = Text(analyzer='standard')

    class Meta:
        '''
        Meta fields: index and doc_type
        '''
        index = 'tamadas_customer_list'
        doc_type = 'customer_list'

    def save(self, ** kwargs):
        '''
        Commit
        '''
        return super().save(** kwargs)


class ProductList(DocType):
    '''
    Class to map process
    '''
    company_id = Integer()
    product_name = Text(analyzer='standard')

    class Meta:
        '''
        Meta fields: index and doc_type
        '''
        index = 'tamadas_product_list'
        doc_type = 'product_list'

    def save(self, ** kwargs):
        '''
        Commit
        '''
        return super().save(** kwargs)


def prepare_business():
    '''
    Prepare mappings
    '''
    connections.create_connection(
        hosts='holly-4618002.us-east-1.bonsaisearch.net', http_auth="2en92fhg:wswukk45favf6krr")
    support = Index("tamadas_cancel")
    if not support.exists():
        #CustomerRevenue.init()
        #CustomerPayment.init()
        #CustomerCluster.init()
        #ServiceLevel.init()
        CustomerCancel.init()
        #VendorList.init()
        #CustomerList.init()
        #ProductList.init()
