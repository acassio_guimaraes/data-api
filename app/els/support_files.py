from elasticsearch_dsl import Index
from elasticsearch_dsl import DocType
from elasticsearch_dsl import Nested
from elasticsearch_dsl import Integer
from elasticsearch_dsl import String
from elasticsearch_dsl.connections import connections
from app import app
# Define a default Elasticsearch client
class MapProcess(DocType):
    '''
    Class to map process
    '''
    company_id = Integer()
    company_name = String()
    process = Nested(properties={
        "file_category": Integer(),
        "process_id": Integer()
    })

    class Meta:
        '''
        Meta fields: index and doc_type
        '''
        index = 'support_platypus'
        doc_type = 'map_process'

    def add_process(self, file_category, process_id):
        '''
        Append nested field
        '''
        self.process.append(
            {"file_category": file_category, "process_id": process_id})

    def save(self, ** kwargs):
        '''
        Commit
        '''
        return super().save(** kwargs)


class MapTransformer(DocType):
    '''
    Class to map the transformation proecss
    '''
    company_id = Integer()
    company_name = String()
    transf_process = Nested(properties={
        "name": String(),
        "transf_id": Integer(),
        "programs": Integer()
    })

    class Meta:
        '''
        Meta fields: index and doc_type
        '''
        index = 'support_platypus'
        doc_type = 'map_transform'

    def add_transf_process(self, name, transf_id, list_programs):
        '''
        Append nested field
        '''
        self.transf_process.append(
            {"name": name, "transf_id": transf_id, "programs": list_programs})

    def save(self, ** kwargs):
        '''
        Commit
        '''
        return super().save(** kwargs)


class ProcessStatus(DocType):
    '''
    Class to map the status of each transformation process
    '''
    id_empresa = Integer()
    nome_empresa = String()
    transform = Nested(properties={
        "name": String(),
        "process_id": Integer(),
        "programs": Integer(),
        "status": Integer()
    })

    class Meta:
        '''
        Meta fields: index and doc_type
        '''
        index = 'support_platypus'
        doc_type = 'process_status'

    def add_transf_process(self, name, process_id, list_programs, status):
        '''
        Append nested field
        '''
        self.transf_process.append(
            {"name": name, "process_id": process_id,
             "programs": list_programs, "status": status})

        def save(self, ** kwargs):
            '''
            Commit
            '''
            return super().save(** kwargs)

def prepare_support():
    '''
    Prepare mappings
    '''
    connections.create_connection(hosts='localhost:9200')
    support = Index("support_platypus")
    if not support.exists():
        MapProcess.init()
        MapTransformer.init()
        ProcessStatus.init()
