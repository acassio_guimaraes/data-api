from app import app
from app.els import CustomerRevenue
from app.els import prepare_business
import datetime as dt
from elasticsearch_dsl.connections import connections
from app.els.search_dict import get_dict_to_products


def return_top_product(vendor, customer, product):
    dict_return = get_dict_to_products(customer, vendor, product)

    con = connections.create_connection(
        hosts='holly-4618002.us-east-1.bonsaisearch.net', http_auth="2en92fhg:wswukk45favf6krr")

    search = CustomerRevenue.search().using(con).from_dict(dict_return)
    results = search.execute()
    info_return = dict()
    info_return["title"] = "Produtos"
    info_return["subtitle"] = "Lista de produtos com maior faturamento nos últimos 12 meses"
    info_return["labels"] = ["Produto", "Faturamento (R$)"]
    info_return["series"] = list()

    for info in results.aggregations.revenue_by_product.buckets:
        info_return["series"].append({"name": info.key, "revenue": '{:,.2f}'.format(
            info.revenue.value).replace(".", "%").replace(",", ".").replace("%", ",")})

    return info_return
