from app import app
from app.els import CustomerPayment
from app.els import prepare_business
import datetime as dt
from elasticsearch_dsl.connections import connections
from app.els.search_dict import get_dict_payment


def query_customer_payment(vendor, customer):
    dict_return = get_dict_payment(customer, vendor)

    con = connections.create_connection(
        hosts='holly-4618002.us-east-1.bonsaisearch.net', http_auth="2en92fhg:wswukk45favf6krr")

    search = CustomerPayment.search().using(con).from_dict(dict_return)
    results = search.execute()
    chart_one = dict()
    chart_one["title"] = "Pagamento"
    chart_one["subtitle"] = "Pagamento Mensal por Faixa de Atraso"
    chart_one["series"] = list()
    chart_one["lineLabel"] = list()
    chart_two = dict()
    chart_two["title"] = "Saldo"
    chart_two["subtitle"] = "Devido vs. Pago"
    chart_two["series"] = list()
    chart_two["lineLabel"] = list()

    series_names = ["em_dia", "fx_5_a_15", "fx_15_a_30", "fx_30_a_60",
                    "fx_60_a_120", "fx_120_a_180", "fx_180_a_360", "fx_maior_360"]
    for item in series_names:
        chart_one["series"].append(
            {"label": item, "type": "bar", "stacked": True, "axis": "primary", "fill": False, "data": []})

    chart_two["series"].append({"label": "Saldo", "type": "line",
                                "stacked": False, "axis": "primary", "fill": False, "data": []})
    chart_two["series"].append({"label": "Valor Pago", "type": "line",
                                "stacked": False, "axis": "primary", "fill": False, "data": []})

    for info in results.aggregations.payment_over_time.buckets:
        date = dt.datetime.strptime(
            info.key_as_string[0:10], "%Y-%m-%d").date()
        chart_two["lineLabel"].append(
            "{}/{}".format(app.config['CALENDAR_PORTUGUESE'][date.month], date.year))
        chart_one["lineLabel"].append(
            "{}/{}".format(app.config['CALENDAR_PORTUGUESE'][date.month], date.year))

        for k, item in enumerate(series_names):
            chart_one["series"][k]["data"].append(getattr(info,"s_{}".format(item)).value)
        
        chart_two["series"][0]["data"].append(getattr(info,"s_{}".format("balance")).value)
        chart_two["series"][1]["data"].append(getattr(info,"s_{}".format("paid_value")).value)        
            
    return chart_one, chart_two
