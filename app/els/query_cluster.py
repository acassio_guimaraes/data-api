from app import app
from app.els import CustomerCluster
from app.els import prepare_business
import datetime as dt
from elasticsearch_dsl.connections import connections
from app.els.search_dict import get_dict_cluster


def return_customer_cluster(vendor, customer):
    dict_return = get_dict_cluster(customer, vendor)

    con = connections.create_connection(
        hosts='holly-4618002.us-east-1.bonsaisearch.net', http_auth="2en92fhg:wswukk45favf6krr")

    search = CustomerCluster.search().using(con).from_dict(dict_return)
    results = search.execute()
    info_return = dict()
    info_return["title"] = "Classificação"
    info_return["subtitle"] = "Classificação baseada em histórico de compras e pagamentos"
    info_return["labels"] = ["Cliente", "Classificação"]
    info_return["series"] = list()
   
    for info in results.hits:
        info_return["series"].append({"name":info.customer_name, "rating":info.customer_cluster})

    return info_return
