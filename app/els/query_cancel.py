from app import app
from app.els import CustomerCancel
from app.els import prepare_business
import datetime as dt
from elasticsearch_dsl.connections import connections
from app.els.search_dict import get_dict_cancel


def query_customer_cancel(vendor, customer, product):
    dict_return = get_dict_cancel(customer, vendor, product)

    con = connections.create_connection(
        hosts='holly-4618002.us-east-1.bonsaisearch.net', http_auth="2en92fhg:wswukk45favf6krr")

    search = CustomerCancel.search().using(con).from_dict(dict_return)
    results = search.execute()
    info_return = dict()
    info_return["title"] = "Programação e Cancelamento"
    info_return["subtitle"] = "Quantidade programada e cancelada ao longo do tempo"
    info_return["series"] = list()
    info_return["lineLabel"] = list()

    info_return["series"].append({"label": "Programado", "type": "bar",
                                  "stacked": False, "axis": "primary", "fill": False, "data": []})
    info_return["series"].append({"label": "Cancelado", "type": "bar",
                                  "stacked": False, "axis": "primary", "fill": False, "data": []})
    
    for info in results.aggregations.cancel_over_time.buckets:
        date = dt.datetime.strptime(
            info.key_as_string[0:10], "%Y-%m-%d").date()
        info_return["lineLabel"].append(
            "{}/{}".format(app.config['CALENDAR_PORTUGUESE'][date.month], date.year))

        info_return["series"][0]["data"].append(info.planejado.value)
        info_return["series"][1]["data"].append(info.cancelado.value)

    return info_return
