from app import app
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search


def return_autofill_list():
    con = connections.create_connection(
        hosts='holly-4618002.us-east-1.bonsaisearch.net', http_auth="2en92fhg:wswukk45favf6krr")

    dict_to_return = {"title": "lists", "series": []}
    docs = {"product_list": ["product_name","tamadas_product_list"],
            "customer_list":["customer_name", "tamadas_customer_list"], "vendor_list": ["vendor_name", "tamadas_vendor_list"]}

    for doc in docs:
        search = Search(using=con, index=docs[doc][1], doc_type=doc)
        search = search.source([docs[doc][0]])
        search_return = search.scan()
        information = [getattr(s, docs[doc][0]) for s in search]
        dict_to_return["series"].append({"name": doc, "data": information})

    return dict_to_return