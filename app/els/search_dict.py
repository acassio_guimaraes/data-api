def get_dict_service_level(info=None):
    if info is None or info == "":
        query = {"must": {"match_all": {}}, "filter": {"range": {
            "date": {"gte": "01/01/2016", "lt": "30/09/2030", "format": "dd/MM/yyyy"}}}}

    else:
        query = {"must": {"match_phrase": {"product_name": {
            "query": str(info).lower(), "analyzer": "standard", "slop": 100}}}, "filter": {
            "range": {"date": {"gte": "01/01/2016", "lt": "30/09/2030", "format": "dd/MM/yyyy"}}}}

    dict_return = {"query": {"bool": query}, "aggs": {"service_over_time": {"date_histogram": {"field": "date", "interval": "month"},
                                                                            "aggs": {"semeados": {"sum": {"field": "value_seed"}}, "entregues": {"sum": {"field": "value_final"}}}}}}
    return dict_return


def get_dict_revenue(customer=None, vendor=None, product=None):
    query = list()
    if product is not None and product != "":
        query.append({"match_phrase": {"product_name": {"query": str(product).lower(),
                                                        "analyzer": "standard", "slop": 100}}})

    if customer is not None and customer != "":
        query.append({"match_phrase": {"customer_name": {"query": str(customer).lower(),
                                                         "analyzer": "standard", "slop": 100}}})

    if vendor is not None and vendor != "":
        query.append({"match_phrase": {"vendor_name": {"query": str(vendor).lower(),
                                                       "analyzer": "standard", "slop": 100}}})

    if len(query) == 0:
        query = {"match_all": {}}

    dict_return = {"query": {"bool": {"must": query, "filter": {"range": {
        "date": {"gte": "01/01/2015", "lt": "30/09/2030", "format": "dd/MM/yyyy"}}}}},
        "aggs": {"revenue_over_time": {"date_histogram": {
            "field": "date", "interval": "month"}, "aggs": {"revenue": {"sum": {"field": "value"}}}}}}

    return dict_return


def get_dict_payment(customer=None, vendor=None):
    query = list()

    if customer is not None and customer != "":
        query.append({"match_phrase": {"customer_name": {"query": str(customer).lower(),
                                                         "analyzer": "standard", "slop": 100}}})

    if vendor is not None and vendor != "":
        query.append({"match_phrase": {"vendor_name": {"query": str(vendor).lower(),
                                                       "analyzer": "standard", "slop": 100}}})

    if len(query) == 0:
        query = {"match_all": {}}

    dict_return = {"query": {"bool": {"must": query, "filter": {"range": {
        "date": {"gte": "01/01/2015", "lt": "30/09/2030", "format": "dd/MM/yyyy"}}}}},
        "aggs": {"payment_over_time": {"date_histogram": {
            "field": "date", "interval": "month"}, "aggs": {"s_em_dia": {"sum": {"field": "em_dia"}}, "s_fx_5_a_15": {"sum": {"field": "fx_5_a_15"}},
                                                            "s_fx_15_a_30": {"sum": {"field": "fx_15_a_30"}}, "s_fx_30_a_60": {"sum": {"field": "fx_30_a_60"}}, "s_fx_60_a_120": {"sum": {"field": "fx_60_a_120"}},
                                                            "s_fx_120_a_180": {"sum": {"field": "fx_120_a_180"}}, "s_fx_180_a_360": {"sum": {"field": "fx_180_a_360"}}, "s_fx_maior_360": {"sum": {"field": "fx_maior_360"}},
                                                            "s_balance": {"sum": {"field": "balance"}}, "s_paid_value": {"sum": {"field": "paid_value"}}}}}}

    return dict_return


def get_dict_cluster(customer=None, vendor=None):
    query = list()

    if customer is not None and customer != "":
        query.append({"match_phrase": {"customer_name": {"query": str(customer).lower(),
                                                         "analyzer": "standard", "slop": 100}}})

    if vendor is not None and vendor != "":
        query.append({"match_phrase": {"vendor_name": {"query": str(vendor).lower(),
                                                       "analyzer": "standard", "slop": 100}}})

    if len(query) == 0:
        query = {"match_all": {}}

    dict_return = {"query": {"bool": {"must": query}}, "sort": {
        "customer_factor": {"order": "desc"}}, "size": 20}

    return dict_return


def get_dict_to_products(customer=None, vendor=None, product=None):
    query = list()
    if product is not None and product != "":
        query.append({"match_phrase": {"product_name": {"query": str(product).lower(),
                                                        "analyzer": "standard", "slop": 100}}})

    if customer is not None and customer != "":
        query.append({"match_phrase": {"customer_name": {"query": str(customer).lower(),
                                                         "analyzer": "standard", "slop": 100}}})

    if vendor is not None and vendor != "":
        query.append({"match_phrase": {"vendor_name": {"query": str(vendor).lower(),
                                                       "analyzer": "standard", "slop": 100}}})

    if len(query) == 0:
        query = {"match_all": {}}

    dict_return = {"query": {"bool": {"must": query, "filter": {"range": {"date": {
        "gte": "now-13M/d", "lt": "now", "format": "dd/MM/yyyy"}}}}},
        "aggs": {"revenue_by_product": {"terms": {"field": "product_keyword", "order": {"revenue": "desc"}, "size": 20},
                                        "aggs": {"revenue": {"sum": {"field": "value"}}}}}}

    return dict_return


def get_dict_cancel(customer=None, vendor=None, product=None):
    query = list()
    if product is not None and product != "":
        query.append({"match_phrase": {"product_name": {"query": str(product).lower(),
                                                        "analyzer": "standard", "slop": 100}}})

    if customer is not None and customer != "":
        query.append({"match_phrase": {"customer_name": {"query": str(customer).lower(),
                                                         "analyzer": "standard", "slop": 100}}})

    if vendor is not None and vendor != "":
        query.append({"match_phrase": {"vendor_name": {"query": str(vendor).lower(),
                                                       "analyzer": "standard", "slop": 100}}})

    if len(query) == 0:
        query = {"match_all": {}}

    dict_return = {"query": {"bool": {"must": query, "filter": {"range": {"date": {"gte": "01/01/2016", "lt": "30/09/2030", "format": "dd/MM/yyyy"}}}}},
                   "aggs": {"cancel_over_time": {"date_histogram": {"field": "date", "interval": "month"},
                                                 "aggs": {"planejado": {"sum": {"field": "plan_qt"}}, "cancelado": {"sum": {"field": "canceled_qt"}}}}}}

    return dict_return
