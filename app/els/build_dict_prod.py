def build_dict_prod(field):
    dict_return = {"query": {"match": {"product_name": {
            "query": field, "operator": "and"}}}, "aggs": {"service_over_time": {
                "date_histogram": {"field": "date", "interval": "month"},
                "aggs": {"semeados": {"sum": {"field": "value_seed"}}, "entregues": {
                    "sum": {"field": "value_final"}}, "division": {"bucket_script": {
                        "buckets_path": {"my_var1": "entregues", "my_var2": "semeados"},
                        "script": "params.my_var1 / params.my_var2"}}}}}}
    return dict_return