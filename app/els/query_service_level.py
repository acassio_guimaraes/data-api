from app import app
from app.els import ServiceLevel
from app.els import prepare_business
from app.els.search_dict import get_dict_service_level
import datetime as dt
from elasticsearch_dsl.connections import connections


def query_service_level(info):
    dict_return = get_dict_service_level(info)

    con = connections.create_connection(
        hosts='holly-4618002.us-east-1.bonsaisearch.net', http_auth="2en92fhg:wswukk45favf6krr")

    search = ServiceLevel.search().using(con).from_dict(dict_return)
    results = search.execute()
    info_return = dict()
    info_return["title"] = "Nível de Serviço"
    info_return["subtitle"] = "Planejado vs. Entregue"
    info_return["lineLabel"] = [app.config['CALENDAR_PORTUGUESE'][i]
                                for i in range(1, 13)]
    info_return["series"] = list()
    index = 0

    for k, info in enumerate(results.aggregations.service_over_time.buckets):
        date = dt.datetime.strptime(
            info.key_as_string[0:10], "%Y-%m-%d").date()

        if k == 0 or year != date.year:
            year = date.year
            info_return["series"].append(
                {"label": str(year), "type": "line", "stacked": False, "axis": "primary", "fill": False, "data": [], })
            index += 1

        sem = info.semeados.value
        ent = info.entregues.value
        difference = date.month - \
            len(info_return["series"][index - 1]["data"]) - 1

        if difference > 0:
            for _ in range(difference):
                info_return["series"][index - 1]["data"].append(None)

        info_return["series"][index - 1]["data"].append(
            min(ent / sem if sem > 0 else 1, 1))

    return info_return
