from elasticsearch_dsl import Index
from elasticsearch_dsl import DocType
from elasticsearch_dsl import Nested
from elasticsearch_dsl import Integer
from elasticsearch_dsl import String
from elasticsearch_dsl import Date
from elasticsearch_dsl import Double
from elasticsearch_dsl import Text
from elasticsearch_dsl.connections import connections
from app import app
# Define a default Elasticsearch client


class CustomerRevenue(DocType):
    '''
    Class to map process
    '''
    company_id = Integer()
    foreing_customer_id = Integer()
    customer_name = String()
    vendor_id = Integer()
    foreing_vendor_id = Integer()
    revenue = Nested(properties={
        "date": Date(),
        "value": Double(),
        "product_name": Text(analyzer='standard'),
        "product_agreg": String(),
        "tray": Integer(),
        "product_detail": String()
    })

    class Meta:
        '''
        Meta fields: index and doc_type
        '''
        index = 'tamadas'
        doc_type = 'customer_revenue'

    def add_product(self, date, value, product_name,
                    product_agreg, tray, product_detail):
        '''
        Append nested field
        '''
        self.revenue.append(
            {"file_date": date, "value": value, "product_name": product_name,
             "product_agreg": product_agreg, "tray": tray, "product_detail": product_detail})

    def save(self, ** kwargs):
        '''
        Commit
        '''
        return super().save(** kwargs)


class CustomerPayment(DocType):
    '''
    Class to map process
    '''
    company_id = Integer()
    foreing_customer_id = Integer()
    customer_name = String()
    vendor_id = Integer()
    foreing_vendor_id = Integer()
    payment = Nested(properties={
        "date": Date(),
        "due_value": Double(),
        "paid_value": Double(),
        "balance": Double(),
        "balance_past_due": Double(),
        "balance_target": Double(),
        "past_due_target": Double(),
        "Em_dia": Double(),
        "fx_5_a_15": Double(),
        "fx_15_a_30": Double(),
        "fx_30_a_60": Double(),
        "fx_60_a_120": Double(),
        "fx_120_a_180": Double(),
        "fx_180_a_360": Double(),
        "fx_maior_360": Double()
    })

    class Meta:
        '''
        Meta fields: index and doc_type
        '''
        index = 'tamadas'
        doc_type = 'customer_payment'

    def add_payment(self, date, due_value=0, paid_value=0,
                    balance=0, balance_past_due=0, balance_target=0,
                    past_due_target=0, dia=0, fx_5_a_15=0,
                    fx_15_a_30=0, fx_30_a_60=0, fx_60_a_120=0,
                    fx_120_a_180=0, fx_180_a_360=0, fx_maior_360=0):
        '''
        Append nested field
        '''
        self.payment.append(
            {"date": date, "due_value": due_value, "paid_value": paid_value,
             "balance": balance, "balance_past_due": balance_past_due,
             "balance_target": balance_target, "past_due_target": past_due_target,
             "Em_dia": dia, "fx_5_a_15": fx_5_a_15, "fx_15_a_30": fx_15_a_30,
             "fx_30_a_60": fx_30_a_60, "fx_60_a_120": fx_60_a_120, "fx_120_a_180": fx_120_a_180,
             "fx_180_a_360": fx_180_a_360, "fx_maior_360": fx_maior_360})

    def save(self, ** kwargs):
        '''
        Commit
        '''
        return super().save(** kwargs)


class CustomerCluster(DocType):
    '''
    Class to map process
    '''
    company_id = Integer()
    foreing_customer_id = Integer()
    customer_name = String()
    vendor_id = Integer()
    foreing_vendor_id = Integer()
    customer_cluster = String()

    class Meta:
        '''
        Meta fields: index and doc_type
        '''
        index = 'tamadas'
        doc_type = 'customer_cluster'

    def save(self, ** kwargs):
        '''
        Commit
        '''
        return super().save(** kwargs)


class ServiceLevel(DocType):
    '''
    Class to map process
    '''
    company_id = Integer()
    product = Nested(properties={
        "date": Date(),
        "value": Double(),
        "product_name": Text(analyzer='standard'),
        "product_agreg": String(),
        "tray": Integer(),
        "product_detail": String(),
        "value_seed": Integer(),
        "value_final": Integer()
    })

    class Meta:
        '''
        Meta fields: index and doc_type
        '''
        index = 'tamadas'
        doc_type = 'service_level'

    def add_product(self, date, value, product_name,
                    product_agreg, tray, product_detail,
                    value_seed, value_final):
        '''
        Append nested field
        '''
        self.revenue.append(
            {"file_date": date, "value": value, "product_name": product_name,
             "product_agreg": product_agreg, "tray": tray, "product_detail": product_detail,
             "value_seed": value_seed, "value_final": value_final})

    def save(self, ** kwargs):
        '''
        Commit
        '''
        return super().save(** kwargs)


def prepare_business():
    '''
    Prepare mappings
    '''
    connections.create_connection(hosts='localhost:9200')
    support = Index("tamadas")
    if not support.exists():
        CustomerRevenue.init()
        CustomerPayment.init()
        CustomerCluster.init()
        ServiceLevel.init()
