from collections import OrderedDict
from io import StringIO
import json
from re import findall
from re import sub
import pandas as pd
import datetime as dt
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.pdfinterp import PDFPageInterpreter
from app.tasks import pdfconverter as pdfp


class PdfProdParser():
    '''
    This class converts the pdf file into a padas datagrame
    '''

    def __init__(self, file_in, fields_dict):
        '''
        Constructor, receives a pdf file
        '''
        self.file_path = file_in
        self.fields_dict = fields_dict
        self.data = None

    def __convert_pdf_to_txt(self):
        '''
        This module converts a pdf file into a dataframe
        '''
        rsrcmgr = PDFResourceManager()
        retstr = StringIO()
        codec = 'utf-8'
        laparams = None
        # This positions define the begining and the end of a field
        list_pos = [18.83076922, 27.29059016, 56.33058288, 236.110346, 292.3104944,
                    343.7306426, 396.890879, 422.1508275, 454.4308555, 479.9308368,
                    511.010801, 544.5508114, 571.0107621, 602.090697, 276.0504062,
                    329.9305133, 377.5109272]

        # The fields will be separeted by semicolumns
        device = pdfp.TabularConverter(rsrcmgr, retstr, codec=codec,
                                       laparams=laparams, sep=';', list_pos=list_pos)
        pdf_file = open(self.file_path, 'rb')
        interpreter = PDFPageInterpreter(rsrcmgr, device)
        maxpages = 0
        for current_page in PDFPage.get_pages(pdf_file, maxpages=maxpages, caching=True):
            interpreter.process_page(current_page)

        text_file = retstr.getvalue().splitlines()
        pdf_file.close()
        device.close()
        retstr.close()
        return text_file

    def __load_data(self):
        '''
        This function will build a dict with the parsed pdf
        '''
        text = self.__convert_pdf_to_txt()
        raw_data = list()

        for text_frag in text:
            # Verify wether the line is valid or not
            find = findall(r'\d{7}', text_frag)
            if find:
                info = text_frag.split(';')
                dict_aux = dict()
                for field in self.fields_dict:
                    dict_aux[field] = info[self.fields_dict[field] - 1]

                raw_data.append(dict_aux)

        self.data = pd.DataFrame(raw_data)

    def __clean_data(self, prod_field):
        '''
        Apply some regular expressions and create some new fields
        '''
        def clean_chars(field_to_analyse):
            '''
            Remove unnecessary chars
            '''
            regex_to_apply = {1: [r'TEMP\.', r''],
                              2: [r'EST\.', r''],
                              3: [r'\.', r' '],
                              4: [r'\*', r' '],
                              5: [r'-', r' '],
                              6: [r'\+', r' '],
                              7: [r'SNAP', r'SNAPDRAGON'],
                              8: [r'ONCYDIUM', r'ONCIDIUM'],
                              9: [r'SPATHIPHYLLUM', r'SPATY'],
                              10: [r'GOMPRHENA', r'GOMPHRENA'],
                              11: [r'  +', r' ']}

            for item in regex_to_apply:
                field_to_analyse = sub(regex_to_apply[item][0],
                                       regex_to_apply[item][1], field_to_analyse)

            return field_to_analyse

        def tray_size(field_to_analyse):
            '''
            Extract the size of the tray and make some consistencies
            '''
            tray = '{"BD512":"512", "BD325":"325", "BD288":"288", "BD200":"200",\
                    "BD163":"163", "BD128":"128", "BD98":"98", "BD96":"96",\
                    "BD50":"50", "BD25":"25", "512":"512", "325":"325", "288":"288",\
                    "200":"200", "163":"163", "128":"128", "98":"98", "96":"96", "51":"512",\
                    "50":"50", "32":"325", "28":"288", "25":"25"}'

            tray = json.loads(tray, object_pairs_hook=OrderedDict)
            for item in tray:
                if item in field_to_analyse:
                    return int(tray[item])
            return 0

        def remove_tray(field_to_analyse):
            '''
            Remove the size of the tray from the product name
            '''
            tray = '{"BD512":"512", "BD325":"325", "BD288":"288", "BD200":"200",\
                    "BD163":"163", "BD128":"128", "BD98":"98", "BD96":"96",\
                    "BD50":"50", "BD25":"25", "512":"512", "325":"325", "288":"288",\
                    "200":"200", "163":"163", "128":"128", "98":"98", "96":"96", "51":"512",\
                    "50":"50", "32":"325", "28":"288", "25":"25"}'

            tray = json.loads(tray, object_pairs_hook=OrderedDict)
            for item in tray:
                if item in field_to_analyse:
                    return sub(item, '', field_to_analyse)

            return field_to_analyse
            
        self.data = self.data[self.data.apply(
            lambda x: x["dt_entrega"] != "", axis=1)]

        prod_new = '{}_new'.format(prod_field)
        self.data[prod_new] = self.data[prod_field].apply(clean_chars)
        self.data['tamanho'] = self.data[prod_new].apply(tray_size)
        self.data[prod_new] = self.data[prod_new].apply(remove_tray)
        prod_agreg = '{}_agreg'.format(prod_field)
        self.data[prod_agreg] = self.data[prod_new].apply(
            lambda x: x.split(r' ', 1)[0])
        self.data['variante'] = self.data[[prod_new, prod_agreg]].\
            apply(lambda x: sub(x[1], r'', x[0]), axis=1)

        # removing white spaces in the begining and in the end o a column
        self.data["product_agreg"] = self.data[prod_agreg].apply(
            lambda x: str(x).strip())
        self.data["value_seed"] = self.data["muda_estimada"].apply(
            lambda x: int(str(x).strip().replace(".", "")))
        self.data["value_final"] = self.data["muda_real"].apply(
            lambda x: int(str(x).strip().replace(".", "")))
        self.data["product_detail"] = self.data["variante"].apply(
            lambda x: str(x).strip())
        self.data["product_name"] = self.data[["product_agreg", "product_detail", "tamanho"]].apply(
            lambda x: "{} {} {}".format(x[0], x[1], x[2]), axis=1)
        self.data["date"] = self.data["dt_entrega"].apply(
            lambda x: "{}/{}".format(str(x).strip().split("/")[1], str(x).strip().split("/")[2]))
        self.data.rename(columns={'tamanho': 'tray'}, inplace=True)

        columns_keep = ["product_agreg", "value_seed",
                        "value_final", "product_detail", "product_name", "date", "tray"]

        self.data = self.data[columns_keep]

        self.data = self.data.groupby(['date', 'product_agreg', 'product_detail',
                                       'product_name', 'tray'],
                                      as_index=False)[['value_final', 'value_seed']].sum()
        self.data["date"] = self.data["date"].apply(
            lambda x: dt.datetime.strptime(str(x).strip(), '%m/%Y'))

        self.data = self.data[self.data.apply(
            lambda x: x["date"] <= dt.datetime.today(), axis=1)]
        
    def provider(self, prod_field):
        '''
        This module wiil convert return the pandas data frame
        '''
        self.__load_data()
        self.__clean_data(prod_field)
        return self.data
