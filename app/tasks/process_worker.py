from app import app
from app.tasks.build_celery import make_celery
from app.tasks.pdfprod import PdfProdParser as prod
from app.els import prepare_business
from app.els import ServiceLevel, CustomerCluster
import pandas as pd
import requests
import json
from elasticsearch.helpers import bulk
from elasticsearch_dsl.connections import connections
celery = make_celery(app)


@celery.task()
def save_production():
    "Insert in elasticsearch"
    path = r'C:\Users\Acássio\Google Drive\Tamadas\Output\dados para hortitec\files_system\treated\producao\germinacao_treat.xlsx'
    data_frame = pd.read_excel(path, header=0)
    size = len(data_frame)
    prepare_business()
    dict_pandas = data_frame.to_dict(orient='records')
    con = connections.create_connection(
        hosts='holly-4618002.us-east-1.bonsaisearch.net', http_auth="2en92fhg:wswukk45favf6krr")

    bulk(con, ({'_index': "tamadas_service", '_type': "service_level", '_source': d}
               for d in dict_pandas))


@celery.task()
def save_client_list():
    "Insert in elasticsearch"
    path = r'C:\Users\Acássio\Google Drive\Tamadas\Output\dados para hortitec\files_system\treated\clientes\list_customers.xlsx'
    data_frame = pd.read_excel(path, header=0, sheetname="lista")
    size = len(data_frame)
    prepare_business()
    dict_pandas = data_frame.to_dict(orient='records')
    con = connections.create_connection(
        hosts='holly-4618002.us-east-1.bonsaisearch.net', http_auth="2en92fhg:wswukk45favf6krr")

    bulk(con, ({'_index': "tamadas_customer_list", '_type': "customer_list", '_source': d}
               for d in dict_pandas))


@celery.task()
def save_vendor_list():
    "Insert in elasticsearch"
    path = r'C:\Users\Acássio\Google Drive\Tamadas\Output\dados para hortitec\files_system\treated\vendedor\list_vendors.xlsx'
    data_frame = pd.read_excel(path, header=0, sheetname="lista")
    size = len(data_frame)
    prepare_business()
    dict_pandas = data_frame.to_dict(orient='records')
    con = connections.create_connection(
        hosts='holly-4618002.us-east-1.bonsaisearch.net', http_auth="2en92fhg:wswukk45favf6krr")

    bulk(con, ({'_index': "tamadas_vendor_list", '_type': "vendor_list", '_source': d}
               for d in dict_pandas))


@celery.task()
def save_product_list():
    "Insert in elasticsearch"
    path = r'C:\Users\Acássio\Google Drive\Tamadas\Output\dados para hortitec\files_system\treated\produtos\list_produtos_full_treat.xlsx'
    data_frame = pd.read_excel(path, header=0)
    size = len(data_frame)
    prepare_business()
    dict_pandas = data_frame.to_dict(orient='records')
    con = connections.create_connection(
        hosts='holly-4618002.us-east-1.bonsaisearch.net', http_auth="2en92fhg:wswukk45favf6krr")

    bulk(con, ({'_index': "tamadas_product_list", '_type': "product_list", '_source': d}
               for d in dict_pandas))


@celery.task()
def save_revenue():
    "Insert in elasticsearch"
    path = r'C:\Users\Acássio\Google Drive\Tamadas\Output\dados para hortitec\files_system\treated\faturamento\fat_atu_20170617_full_trat.xlsx'
    data_frame = pd.read_excel(path, header=0)
    size = len(data_frame)
    prepare_business()
    dict_pandas = data_frame.to_dict(orient='records')
    con = connections.create_connection(
        hosts='holly-4618002.us-east-1.bonsaisearch.net', http_auth="2en92fhg:wswukk45favf6krr")

    bulk(con, ({'_index': "tamadas_revenue", '_type': "customer_revenue", '_source': d}
               for d in dict_pandas))


@celery.task()
def save_payment():
    "Insert in elasticsearch"
    path = r'C:\Users\Acássio\Google Drive\Tamadas\Output\dados para hortitec\files_system\treated\pagamentos\pagto_atu_20170617_full_treat.xlsx'
    data_frame = pd.read_excel(path, header=0)
    size = len(data_frame)
    print(size)
    prepare_business()
    dict_pandas = data_frame.to_dict(orient='records')
    con = connections.create_connection(
        hosts='holly-4618002.us-east-1.bonsaisearch.net', http_auth="2en92fhg:wswukk45favf6krr")

    bulk(con, ({'_index': "tamadas_payment", '_type': "customer_payment", '_source': d}
               for d in dict_pandas))


@celery.task()
def save_cluster():
    "Insert in elasticsearch"
    path = r'C:\Users\Acássio\Google Drive\Tamadas\Output\dados para hortitec\files_system\treated\clientes\cluster_atu_201612_treat.xlsx'
    data_frame = pd.read_excel(path, header=0)
    size = len(data_frame)
    prepare_business()
    dict_pandas = data_frame.to_dict()
    for i in range(size):
        info = CustomerCluster(company_id=1, 
                            foreing_customer_id=int(dict_pandas["foreing_customer_id"][i]),
                            customer_name=dict_pandas["customer_name"][i],
                            foreing_vendor_id=int(dict_pandas["foreing_vendor_id"][i]),
                            vendor_name=dict_pandas["vendor_name"][i],
                            customer_cluster=dict_pandas["customer_cluster"][i],
                            customer_factor = dict_pandas["customer_factor"][i])
        info.save()

@celery.task()
def save_cancel():
    "Insert in elasticsearch"
    path = r'C:\Users\Acássio\Google Drive\Tamadas\Output\dados para hortitec\files_system\treated\cancelamento\cancel_prod_cli_atu_201706_2_t_2.xlsx'
    data_frame = pd.read_excel(path, header=0)
    size = len(data_frame)
    print(size)
    prepare_business()
    dict_pandas = data_frame.to_dict(orient='records')
    con = connections.create_connection(
        hosts='holly-4618002.us-east-1.bonsaisearch.net', http_auth="2en92fhg:wswukk45favf6krr")

    bulk(con, ({'_index': "tamadas_cancel", '_type': "customer_cancel", '_source': d}
               for d in dict_pandas))