from app import app
from app.tasks.build_celery import make_celery
from app.tasks.pdfprod import PdfProdParser as prod
from app.els import prepare_business
from app.els import ServiceLevel
import pandas as pd
celery = make_celery(app)


@celery.task()
def run_task():
    '''
    Run the hard work
    '''

    file_in = r'C:\Users\Acássio\Desktop\Consultoria\tamadas\testes\07-12 2015 (2).pdf'

    fields_dict = {"ordem": 1, "semente": 2, "lote": 3, "prod": 4,
                   "dt_lote": 5, "semana_lote": 6, "dt_sem": 7,
                   "semana_sem": 8, "dt_entrega": 9, "semana_entrega": 10,
                   "band_semeada": 11, "muda_semeada": 12, "band_estimada": 13,
                   "muda_estimada": 14, "perc_estimada": 15, "band_real": 16,
                   "muda_real": 17, "perc_real": 18}

    obj = prod(file_in, fields_dict)
    data_frame = obj.provider('prod')

    writer = pd.ExcelWriter(
        r'C:\Users\Acássio\Desktop\Consultoria\tamadas\testes\germina_new_15.xlsx')
    data_frame.to_excel(writer, 'Sheet1')
    writer.save()


@celery.task()
def save_production():
    "Insert in elasticsearch"
    path = r'C:\Users\Acássio\Google Drive\Tamadas\Output\dados para hortitec\files_system\treated\producao\germinacao_treat.xlsx'
    data_frame = pd.read_excel(path, header=0)
    size = len(data_frame)
    prepare_business()
    dict_pandas = data_frame.to_dict()
    for i in range(size):
        info = ServiceLevel(company_id=1, date=dict_pandas["date"][i],
                            product_name=dict_pandas["product_name"][i],
                            product_agreg=dict_pandas["product_agreg"][i],
                            tray=int(dict_pandas["tray"][i]),
                            product_detail=dict_pandas["product_detail"][i],
                            value_seed=int(dict_pandas["value_seed"][i]),
                            value_final=int(dict_pandas["value_final"][i]))
        info.save()

@celery.task()
def query_service_level(json_info):
    dict_return = {"query": {"match": {"product_name": {
        "query": json_info, "operator": "and"}}}, "aggs": {"service_over_time": {
            "date_histogram": {"field": "date", "interval": "month"},
            "aggs": {"semeados": {"sum": {"field": "value_seed"}}, "entregues": {
                "sum": {"field": "value_final"}}, "division": {"bucket_script": {
                    "buckets_path": {"my_var1": "entregues", "my_var2": "semeados"},
                    "script": "params.my_var1 / params.my_var2"}}}}}}

    prepare_business()
    search = ServiceLevel.search()
    search.from_dict(dict_return)
    results = search.execute()
    return results.to_dict
