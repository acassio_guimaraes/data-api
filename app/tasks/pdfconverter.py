
#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pdfminer.converter import PDFConverter
from pdfminer.converter import LTContainer
from pdfminer.converter import LTText
from pdfminer.converter import LTTextBox
from pdfminer.converter import LTImage
from pdfminer.converter import TextConverter


class TabularConverter(TextConverter):
    def __init__(self, rsrcmgr, outfp, codec='utf-8', pageno=1, laparams=None,
                 showpageno=False, imagewriter=None, sep = '\t', list_pos=None):
        PDFConverter.__init__(self, rsrcmgr, outfp, codec=codec, pageno=pageno, laparams=laparams)

        self.showpageno = showpageno
        self.imagewriter = imagewriter
        self.sep = sep
        self.list_pos = list_pos
        return

    def receive_layout(self, ltpage):
        elements = list()
        current = None

        def render(item):
            global current
            if isinstance(item, LTContainer):
                for child in item:
                    render(child)
            elif isinstance(item, LTText):
                if hasattr(item, 'matrix'):
                    current = item.matrix[4:]
                    elements.append(item.matrix[4:] + tuple(item.get_text()))
            if isinstance(item, LTTextBox):
                self.write_text('\n')
            elif isinstance(item, LTImage):
                if self.imagewriter is not None:
                    self.imagewriter.export_image(item)
        if self.showpageno:
            self.write_text('Page %s\n' % ltpage.pageid)
        render(ltpage)

        list_aux = list(set([e[1] for e in elements]))
        list_aux.sort()
        list_tmp = list()

        current = None
        list_tmp.append(list_aux[0])

        for i in list_aux:
            if current:
                if not current - 1.5 <= i <= current + 1.5:
                    list_tmp.append(i)
            current = i

        for e in list_tmp:
            for i in self.list_pos:
                elements.append((i, e, self.sep))

        element_final = list()
        for e in elements:
            for k in list_tmp:
                if (k - 1.5) <= e[1] <= (k + 1.5):
                    a = (e[0], k, e[2])
                    element_final.append(a)

        element_final = sorted(element_final, key=lambda i: (-i[1], i[0]))
        current = None

        for e in element_final:
            if not current or ((current - 1.5) <= e[1] <= (current + 1.5)):
                self.write_text(e[2])
            else:
                self.write_text('\n')
                self.write_text(e[2])
            current = e[1]
        self.write_text('\f')
        return
